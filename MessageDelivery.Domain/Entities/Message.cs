﻿using System;

namespace MessageDelivery.Domain.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set;}
        public string CellNumberRecipient { get; set; }
        public DateTime Created { get; set; }
        public DateTime MessageSent { get; set; }
    }
}
