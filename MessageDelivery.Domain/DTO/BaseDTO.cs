﻿
namespace MessageDelivery.Domain.DTO
{
    public class BaseDTO
    {
        public int Status { get; set; } = (int)StatusFlags.Clean;
    }
}
