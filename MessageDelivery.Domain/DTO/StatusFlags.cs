﻿namespace MessageDelivery.Domain.DTO
{
    public enum StatusFlags
    {
        Clean = 0,
        Dirty = 1,
        Add = 2,
        Delete = 3
    }
}