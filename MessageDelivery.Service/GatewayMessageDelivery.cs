﻿using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.Web;

namespace MessageDelivery.Service
{
    //TODO make generic Gateway 
    public class GatewayMessageDelivery : IGateway
    {
        public GatewayMessageDelivery()
        {

        }

        public string SendSMS(string number, string message, string sender)
        {
            //TODO merge settings with appsettings.json

            //string message = HttpUtility.UrlEncode("Bon dia, laga mi purba e kos esaki");
                using (var wb = new WebClient())
                {
                    byte[] response = wb.UploadValues("http://api.txtlocal.com/send/", new NameValueCollection()
                {
                {"apiKey" , ConfigurationManager.AppSettings["apiKey"]},
                {"username" , ConfigurationManager.AppSettings["username"]},
                {"password" , ConfigurationManager.AppSettings["password"]},
                {"hash", ConfigurationManager.AppSettings["hash"]},
                {"numbers" , "31648541412"},
                {"message" , HttpUtility.UrlEncode(message)},
                {"sender" , sender}
                });
                    string result = System.Text.Encoding.UTF8.GetString(response);
                    return result;
                }
        }
    }
}
