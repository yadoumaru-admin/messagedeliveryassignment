﻿namespace MessageDelivery.Service
{
    public interface IGateway
    {
        string SendSMS(string number, string message, string sender);
    }
}
