﻿using MessageDelivery.Core;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MessageDelivery.Controllers.Core
{
    public class JSendResponse
    {
        private const string SUCCESS = "success";
        private const string FAIL = "fail";
        private const string ERROR = "error";
        private const string KEY_STATUS = "status";
        private const string KEY_DATA = "data";
        private const string KEY_MESSAGE = "message";
        private const string KEY_CODE = "code";

        public string Status;
        public List<object> Data = new List<object>();
        public string ErrorCode;
        public string ErrorMessage;

        public string GetStatus()
        {
            return Status;
        }
        public List<object> GetData()
        {
            return Data;
        }
        public string GetErrorMessage()
        {
            if (IsError())
            {
                return ErrorMessage;
            }

            throw new Exception("Only responses with a status of error may have an error message.");
        }
        public string GetErrorCode()
        {
            if (IsError())
            {
                return ErrorCode;
            }
            throw new Exception("Only responses with a status of error may have an error code.");
        }

        protected bool IsStatusValid(string status)
        {
            var validStatuses = new ArrayList { SUCCESS, FAIL, ERROR };
            return validStatuses.Contains(status);
        }
        public bool IsSuccess()
        {
            return Status == SUCCESS;
        }
        public bool IsFail()
        {
            return Status == SUCCESS;
        }
        public bool IsError()
        {
            return Status == SUCCESS;
        }

        public static JSendResponse Success(object data = null)
        {
            return new JSendResponse(SUCCESS, data);
        }

        public static JSendResponse Fail(object data = null)
        {
            return new JSendResponse(FAIL, data);
        }

        public static JSendResponse Error(string errorMessage, string errorCode = null, object data = null)
        {
            return new JSendResponse(ERROR, data, errorMessage, errorCode);
        }

        public JSendResponse(string status, object data = null, string errorMessage = null, string errorCode = null)
        {
            if (!IsStatusValid(status))
            {
                throw new Exception("Status does not conform to JSend spec.");
            }

            Status = status;

            if (status == ERROR)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    throw new Exception("Errors must contain a message.");
                }
                ErrorMessage = errorMessage;
                ErrorCode = errorCode;
            }
            Data.Add(data);
        }

        public string ToJson()
        {
            return new MessageDelivery.Core.JsonSerializer().Serialize(this);
        }

        public static object FromJson(string json)
        {
            dynamic jsonResponse = JsonConvert.DeserializeObject(json);
            jsonResponse.Works = true;
            return jsonResponse;
        }
    }
}