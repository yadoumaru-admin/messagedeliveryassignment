﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace MessageDelivery.Controllers.Core
{
    public abstract class ApplicationController : Controller
    {
        protected readonly HttpContext Context;
        protected readonly IHttpContextAccessor ContextAccessor;
        protected readonly IMapper Mapper;

        protected ApplicationController(
            IHttpContextAccessor contextAccessor,
            IMapper mapper
        )
        {
            ContextAccessor = contextAccessor;
            Context = contextAccessor.HttpContext;
            Mapper = mapper;
        }


        #region Response methods

        protected string SuccessResponse(object responseBody)
        {
            var actionResponseHeader = GenerateActionResponseHeader();
            var actionResponse = new ActionResponse
            {
                Header = actionResponseHeader,
                Body = responseBody
            };

            return JSendResponse.Success(actionResponse).ToJson();
        }

        protected string FailResponse(object responseBody)
        {
            var actionResponseHeader = GenerateActionResponseHeader();
            var actionResponse = new ActionResponse
            {
                Header = actionResponseHeader,
                Body = responseBody
            };

            return JSendResponse.Fail(actionResponse).ToJson();
        }

        private Header GenerateActionResponseHeader()
        {
            var issuedToken = "";
            string authorization = Context.Request.Headers["Authorization"];
            if (authorization != null && authorization.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                issuedToken = authorization.Substring("Bearer ".Length).Trim();

            var header = new Header
            {
                UserData = new UserData
                {
                    Authentication = new Authentication
                    {
                        BearerToken = issuedToken
                    }
                }
            };
            return header;
        }

        #endregion

        #region Data classes

        public class ActionResponse
        {
            public object Body;
            public Header Header;
        }

        public class Header
        {
            public UserData UserData;
        }

        public class UserData
        {
            public Authentication Authentication;
            public IList<Claim> Claims;
            public IList<string> Roles;
            public string UserId;
            public string UserName;
        }

        public class Authentication
        {
            public string BearerToken;
        }

        #endregion
    }
}