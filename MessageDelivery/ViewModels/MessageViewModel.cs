﻿namespace MessageDelivery.ViewModels
{
    public class MessageViewModel
    {
        public string Onderwerp { get; set; }
        public string Tekst { get; set; }
        public string Afzender { get; set; }
        public string Ontvanger { get; set; }
        public string TelefoonNrOntvanger { get; set; }

    }
}
