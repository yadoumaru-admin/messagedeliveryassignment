﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MessageDelivery.Controllers.Core;
using MessageDelivery.DAL;
using MessageDelivery.Domain.Entities;
using MessageDelivery.Service;
using MessageDelivery.Validators;
using MessageDelivery.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MessageDelivery.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : ApplicationController
    {
        private readonly UnitOfWork _unitOfWork;

        public MessagesController(IHttpContextAccessor contextAccessor, IMapper mapper, UnitOfWork unitOfWork) : base(contextAccessor, mapper)
        {
            _unitOfWork = unitOfWork;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(string id)
        {
            var results = _unitOfWork.MessageRepository.Get(msg => msg.Recipient == id).ToList();
            var models = Mapper.Map<List<Message>,List<MessageViewModel>>(results);
            return SuccessResponse(models);
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody] MessageViewModel model)
        {
            var gateway = new GatewayMessageDelivery();
            var validator = new MessageValidator();
            var message = Mapper.Map<MessageViewModel, Message>(model);

            message.Created = DateTime.Now;
            message.MessageSent = DateTime.Now; //TODO determine logic
            
            if(validator.Validate(message).IsValid)
            {
                _unitOfWork.MessageRepository.Insert(message);
                _unitOfWork.Save();

                gateway.SendSMS("31648541412", message.Body, message.Recipient);


                return SuccessResponse(Mapper.Map<Message, MessageViewModel>(message));   
            }
            return FailResponse("Kan het bericht niet aanmaken");
        }


    }
}