﻿using AutoMapper;
using MessageDelivery.Domain.Entities;
using MessageDelivery.ViewModels;

namespace MessageDelivery
{
    public class DistributieMappingProfile : Profile
    {
        public DistributieMappingProfile()
        {
            CreateMap<Message, MessageViewModel>()
                .ForMember(m => m.Onderwerp, ex => ex.MapFrom(m => m.Subject))
                .ForMember(m => m.Tekst, ex => ex.MapFrom(m => m.Body))
                .ForMember(m => m.Ontvanger, ex => ex.MapFrom(m => m.Recipient))
                .ForMember(m => m.Afzender, ex => ex.MapFrom(m => m.Sender))
                .ForMember(m => m.TelefoonNrOntvanger, ex => ex.MapFrom(m => m.CellNumberRecipient))
                .ReverseMap();
        }
    }
}
