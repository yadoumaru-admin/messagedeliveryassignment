﻿using FluentValidation;
using MessageDelivery.Domain.Entities;

namespace MessageDelivery.Validators
{
    public class MessageValidator : AbstractValidator<Message>
    {
        public MessageValidator()
        {
            //TODO valideren op speciale characters 
            RuleFor(m => m.Subject).NotEmpty().WithMessage("Geen onderwerp ingevuld");
            RuleFor(m => m.Body).NotEmpty().WithMessage("Geen body ingevuld");
        }

    }
}
