This assignment consists of building a message delivery feature that could send
updates to users and also keep track of the updates sent for later processing.
Requirements
#
#

	• Create an API to post a message which can be delivered to a recipient via
		sms/email and keep a record of it
	• Create an API to retrieve the messages sent to a particular recipient
		Optional
	• Think about how you can secure the above feature