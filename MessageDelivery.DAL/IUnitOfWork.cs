﻿namespace MessageDelivery.DAL
{
    public interface IUnitOfWork
    {
        void Save();
        void Dispose();
    }
}
