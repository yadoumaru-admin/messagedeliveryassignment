﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MessageDelivery.DAL.Migrations
{
    public partial class UpdateMessages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CellNumberRecipient",
                table: "Messages",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Sender",
                table: "Messages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CellNumberRecipient",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "Sender",
                table: "Messages");
        }
    }
}
