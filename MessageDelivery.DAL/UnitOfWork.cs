﻿using MessageDelivery.Domain.Entities;
using System;

namespace MessageDelivery.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly MsgDeliveryContext _context = new MsgDeliveryContext();

        private bool _disposed;
        private GenericRepository<Message> _messageRepository;

        public GenericRepository<Message> MessageRepository => _messageRepository ?? (_messageRepository =
            new GenericRepository<Message>(_context));

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();
            _disposed = true;
        }
    }
}
