﻿using MessageDelivery.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace MessageDelivery.DAL
{
    public class MsgDeliveryContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }
        public MsgDeliveryContext()
        {
        }

        public MsgDeliveryContext(DbContextOptions<MsgDeliveryContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=localhost;Database=MessageDelivery;Integrated Security=True;");
            }
        }

    }
}
